{{/* _helpers.tpl */}}
{{- define "augmenta-app.fullname" -}}
{{- printf "%s-%s" .Release.Name .Chart.Name }}
{{- end -}}

{{- define "augmenta-app.labels" -}}
app.kubernetes.io/name: {{ include "augmenta-app.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}

{{- define "augmenta-app.selectorLabels" -}}
app.kubernetes.io/name: {{ include "augmenta-app.name" . }}
{{- end -}}

{{- define "augmenta-app.name" -}}
{{- default "augmenta-app" .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}
