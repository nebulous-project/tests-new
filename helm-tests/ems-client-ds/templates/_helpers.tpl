{{/* _helpers.tpl */}}
{{- define "ems-client-daemonset.fullname" -}}
{{- printf "%s-%s" .Release.Name .Chart.Name }}
{{- end -}}

{{- define "ems-client-daemonset.labels" -}}
app.kubernetes.io/name: {{ include "ems-client-daemonset.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}

{{- define "ems-client-daemonset.selectorLabels" -}}
app.kubernetes.io/name: {{ include "ems-client-daemonset.name" . }}
{{- end -}}

{{- define "ems-client-daemonset.name" -}}
{{- default "ems-client-daemonset" .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}
